import mongoose from 'mongoose';

const URI = 'mongodb://localhost/apigraphqlyoga'
mongoose.connect(URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
.then(db => console.log('db is connected'))
.catch(err => console.log('something go wrong'));